<?php
$start = microtime(true);
$sum = 0;
for ($i = 0; $i < 20000000; $i++) {
    $sum += rand(0, 78987);
}
echo "<span style='color: blue;font-size: 18px;font-weight: bold;'>[rand] Time:</span>" . (microtime(true) - $start) . '<br/>';

$start = microtime(true);
$sum = 0;
for ($i = 0; $i < 20000000; $i++) {
    $sum += mt_rand(0, 78987);
}
echo "<span style='color: blue;font-size: 18px;font-weight: bold;'>[mt_rand] Time:</span>" . (microtime(true) - $start) . '<br/>';


$start = microtime(true);
$sum = 0;
for ($i = 0; $i < 20000000; $i++) {
    $sum += random_int(0, 78987);
}
echo "<span style='color: blue;font-size: 18px;font-weight: bold;'>[random_int] Time:</span>" . (microtime(true) - $start) . '<br/>';
