<?php
if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
    list($op1, $op2, $operator) = array(intval($_POST['op1']), intval($_POST['op2']), intval($_POST['operator']));
    switch ($operator) {
        case '+':
            echo $op1 + $op2;
            break;
        case '-':
            echo $op1 - $op2;
            break;
        case '*':
            echo $op1 * $op2;
            break;
        default:
            echo "Invalid OPerator";
    }
}

