<?php

$arr = [
    ["Ali", "age" => 29, "weight" => 70],
    ["Mohammad", "age" => 20, "weight" => 65],
    ["Hassan", "age" => 35, "weight" => 65]

];
usort($arr, function ($arrA, $arrB) {
    if ($arrA["weight"] <=> $arrB["weight"]) {
        return $arrA["weight"] <=> $arrB["weight"];
    } else {
        return $arrA["age"] <=> $arrB["age"];
    }
});
echo "<pre>";
print_r($arr);
echo "</pre>";