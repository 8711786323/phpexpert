<?php
function myPrint($var = NULL, $print_type = 'line')
{
    if (is_array($var)) {
        printArr($var);

    } else {
        $red_rand = rand(0, 255);
        $green_rand = rand(0, 255);
        $blue_rand = rand(0, 255);
        if ($print_type == 'line') {

            echo "<div style='border: 1px solid rgb($red_rand, $green_rand, $blue_rand);background: #f3f3f3;padding: 5px; border-radius: 5px;margin: 10px 20px;'>$var</div>";
        } else if ($print_type == 'inline') {

            echo "<div style='border: 1px solid rgb($red_rand, $green_rand, $blue_rand);float: left;width: 100px; background: #f3f3f3;padding: 5px; border-radius: 5px;margin:5px;'>$var</div>";

        }

    }

}

function sum($a, $b)
{
    $args = func_get_args();
    return sumArray($args);
}

function sumNew(... $args)
{
    return sumArray($args);
}

function sumArray(&$arr)
{

    $sum = 0;
    foreach ($arr as $item) {
        $sum += $item;
    }
    return $sum;

}

function getVarName($var)
{
    foreach ($GLOBALS as $key => $value) {
        if ($var == $value) {
            return $key;
        }

    }
}

function niceDump(...$args)
{
    echo "<pre>";
    foreach ($args as $arg) {
        var_dump($arg);
        echo "<hr style='border: 1px dashed #e2e2e2;'/>";
    }

    echo "</pre>";
}

function print2DArr($arr)
{
    foreach ($arr as $k => $v) {
        if (is_array($v)) {
            foreach ($v as $k2 => $v2) {
                myPrint("$k2 : $v2");
            }
        } else {
            myPrint("$k : $v");
        }
    }
}

function printArr($arr)
{
    foreach ($arr as $k => $v) {
        if (is_array($v)) {
            printArr($v);
        } else {
            myPrint("$k : $v", 'inline');
        }
    }
    echo "<div style='clear: both'>";
}

